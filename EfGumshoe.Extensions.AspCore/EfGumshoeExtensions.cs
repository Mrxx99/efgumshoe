﻿using EfGumshoe.Plugin;
using EfGumshoe.Plugin.InterProcessCommunication;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;

namespace EfGumshoe.Extensions.AspCore;



public static class EfGumshoeExtensions
{
    public static void AddEfGumshoe(this IServiceCollection services)
    {
        services.AddScoped<IPipeClient>(
            serviceProvider =>
                new PipeClient(
                    serviceProvider.GetService<ILogger<PipeClient>>() ??
                    NullLogger<PipeClient>.Instance
                )
        );

        services.AddScoped<IDbStatementInterceptor, DbStatementInterceptor>();
    }



    public static DbContextOptionsBuilder UseEfGumshoe(
        this DbContextOptionsBuilder optionsBuilder,
        IServiceProvider serviceProvider,
        bool condition
    )
    {
        return
            !condition
                ? optionsBuilder
                : optionsBuilder.AddInterceptors(serviceProvider.GetRequiredService<IDbStatementInterceptor>());
    }
}
