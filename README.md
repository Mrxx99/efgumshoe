# EF Gumshoe

---

EF Gumshoe is a Windows desktop app which provides an overview over the SQL queries
[Entity Framework Core](https://github.com/dotnet/efcore) generates.

This helps identifying issues like comparably slow queries, or [N+1 errors](https://duckduckgo.com/?q=N%2B1+error&t=h_&ia=web)
more easily than with the logging output EF provides out of the box.


## Requirements

- .NET 6.0 and up
- Windows 10 and up for the desktop application


## Installation

Install [Ef Gumshoe with NuGet](https://www.nuget.org/packages/EfGumshoe.Extensions.AspCore/):

```
Install-Package EfGumshoe.Extensions.AspCore
```

Or via the .NET Core command line interface:

```
dotnet add package EfGumshoe.Extensions.AspCore
```


## Usage

Add EF Gumshoe to your dependencies:
```csharp
builder.Services.AddEfGumshoe();
```

Then use it when setting up your DB:

```csharp
builder.Services
    .AddDbContext<MyDbContext>(
        (provider, options) =>
            options
                .UseEfGumshoe(provider, builder.Environment.IsDevelopment())
    );
```

SQL statements sent by the web app should then be sent to the desktop app.

> ❗ The second parameter for `UseEfGumshoe()` is a `bool` that prevents EF Gumshoe from running if it is `false`.  
 It is _highly_ recommended to not run EF Gumshoe in production, like in the example above.


## Wiki

Downloads for the desktop app and more documentation can be found in
[the wiki](https://gitlab.com/raphaelschmitz00/efgumshoe/-/wikis).
