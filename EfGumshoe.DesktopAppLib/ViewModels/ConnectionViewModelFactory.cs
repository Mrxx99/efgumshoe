﻿using EfGumshoe.DesktopAppLib.Services;

namespace EfGumshoe.DesktopAppLib.ViewModels;



public interface IConnectionViewModelFactory
{
    ConnectionViewModel Create(int connectionHashCode);
}



public class ConnectionViewModelFactory : IConnectionViewModelFactory, ISingletonDependency
{
    private readonly IClock _clock;



    public ConnectionViewModelFactory(IClock clock)
    {
        _clock = clock;
    }



    private static int IdCounter { get; set; } = 1;



    public ConnectionViewModel Create(int connectionHashCode) =>
        new(IdCounter++, connectionHashCode, _clock.GetNow());
}
