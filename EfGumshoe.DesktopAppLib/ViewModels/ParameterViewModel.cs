﻿namespace EfGumshoe.DesktopAppLib.ViewModels;



public class ParameterViewModel
{
    public ParameterViewModel(string name, object? value)
    {
        Name = name;
        Value = value;
    }



    public string Name { get; }

    public object? Value { get; }
}
