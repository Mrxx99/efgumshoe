﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Input;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using EfGumshoe.DesktopAppLib.Services;

namespace EfGumshoe.DesktopAppLib.ViewModels;



public interface IMainWindowViewModel
{
    ConnectionViewModel? GetByHashCode(int hashCode);
    void AddConnection(ConnectionViewModel connectionViewModel);
}



public class MainWindowViewModel : ObservableObject, IMainWindowViewModel, ISingletonDependency
{
    private readonly IConnectionDeleteConfirmer _connectionDeleteConfirmer;



    public MainWindowViewModel(IConnectionDeleteConfirmer connectionDeleteConfirmer)
    {
        _connectionDeleteConfirmer = connectionDeleteConfirmer;

        DeleteSelectedConnectionsCommand = new RelayCommand<object>(DeleteSelectedConnections);
        DeleteAllConnectionsCommand = new RelayCommand(DeleteAllConnections);
    }



    public ICommand DeleteSelectedConnectionsCommand { get; }
    public ICommand DeleteAllConnectionsCommand { get; }


    public IMainWindowViewModel ConnectionOverviewViewModel => this;


    public ObservableCollection<ConnectionViewModel> Connections { get; } = new();


    private ConnectionViewModel? _selectedConnection;
    public ConnectionViewModel? SelectedConnection
    {
        get => _selectedConnection;
        set => SetProperty(ref _selectedConnection, value);
    }



    public ConnectionViewModel? GetByHashCode(int hashCode) =>
        Connections.FirstOrDefault(x => x.HashCode == hashCode);



    public void AddConnection(ConnectionViewModel connectionViewModel)
    {
        Connections.Add(connectionViewModel);
        SelectedConnection ??= connectionViewModel;
    }



    private void DeleteSelectedConnections(object? obj)
    {
        var selectedConnections = (IList)obj!;

        var castSelectedConnections =
            selectedConnections
                .Cast<ConnectionViewModel>()
                .ToList();

        var warningResult = _connectionDeleteConfirmer.ShowDeleteWarning(castSelectedConnections);
        if (warningResult != IConnectionDeleteConfirmer.Result.Confirmed) return;


        foreach (var selectedConnection in castSelectedConnections)
        {
            Connections.Remove(selectedConnection);
        }
    }



    private void DeleteAllConnections()
    {
        var warningResult = _connectionDeleteConfirmer.ShowDeleteAllWarning();
        if (warningResult != IConnectionDeleteConfirmer.Result.Confirmed) return;

        Connections.Clear();
    }
}
