﻿using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;

namespace EfGumshoe.DesktopAppLib.ViewModels;



public class ConnectionViewModel : ObservableObject
{
    public ConnectionViewModel(int id, int hashCode, DateTime creationDate)
    {
        Id = id;
        HashCode = hashCode;
        CreationDate = creationDate;
    }



    public int Id { get; }
    public int HashCode { get; }
    public DateTime CreationDate { get; }


    public ObservableCollection<TransactionViewModel> Transactions { get; } = new();


    public ObservableCollection<StatementViewModel> Statements { get; } = new();


    private StatementViewModel? _selectedStatement;
    public StatementViewModel? SelectedStatement
    {
        get => _selectedStatement;
        set => SetProperty(ref _selectedStatement, value);
    }



    public TransactionViewModel? GetTransactionByHashCode(int? hashCode) =>
        Transactions.FirstOrDefault(x => x.TransactionHashCode == hashCode);



    public void AddTransaction(TransactionViewModel transaction) => Transactions.Add(transaction);



    public void AddStatement(StatementViewModel statementViewModel)
    {
        Statements.Add(statementViewModel);
        SelectedStatement ??= statementViewModel;
    }
}
