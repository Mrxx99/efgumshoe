﻿namespace EfGumshoe.DesktopAppLib.ViewModels;



public class StatementViewModel
{
    public StatementViewModel(
        TransactionViewModel? transaction,
        string? commandText,
        DateTime? sentDate,
        List<ParameterViewModel> parameters,
        int rowCount,
        TimeSpan duration
    )
    {
        Transaction = transaction;
        CommandText = commandText;
        SentDate = sentDate;
        Parameters = parameters;
        RowCount = rowCount;
        Duration = duration;
    }



    public TransactionViewModel? Transaction { get; }
    public string TransactionText => Transaction == null ? "" : $"#{Transaction.Id}";


    public string? CommandText { get; }
    public string? CommandTextShort => CommandText?.Replace(Environment.NewLine, "");


    public int RowCount { get; }


    public TimeSpan Duration { get; }

    public string DurationText
    {
        get
        {
            if (Duration > TimeSpan.FromSeconds(9)) return $"{Duration.TotalSeconds:F0} s";
            if (Duration > TimeSpan.FromMilliseconds(9)) return $"{Duration.TotalMilliseconds:F0} ms";
            return $"{Duration.TotalMilliseconds:F2} ms";
        }
    }


    public DateTime? SentDate { get; }
    public string? SentDateText => SentDate?.ToString("HH:mm:ss");


    public List<ParameterViewModel> Parameters { get; }
}
