﻿namespace EfGumshoe.DesktopAppLib.ViewModels;



public class TransactionViewModel
{
    public TransactionViewModel(int transactionHashCode)
    {
        TransactionHashCode = transactionHashCode;
    }



    private static int IdCounter { get; set; } = 1;


    public int Id { get; } = IdCounter++;

    public int TransactionHashCode { get; }
}
