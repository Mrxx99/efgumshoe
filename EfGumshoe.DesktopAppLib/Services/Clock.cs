﻿namespace EfGumshoe.DesktopAppLib.Services;



public interface IClock
{
    public DateTime GetNow();
}



public class Clock : IClock, ITransientDependency
{
    public DateTime GetNow() => DateTime.Now;
}
