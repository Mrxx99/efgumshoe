﻿using EfGumshoe.DesktopAppLib.ViewModels;

namespace EfGumshoe.DesktopAppLib.Services;



public interface IDeleteConnectionWarningFormatter
{
    string ComposeWarningMessage(IEnumerable<ConnectionViewModel> connections);
}



public class DeleteConnectionWarningFormatter : IDeleteConnectionWarningFormatter, ITransientDependency
{
    public string ComposeWarningMessage(IEnumerable<ConnectionViewModel> connections) =>
        $"Do you really want to remove connections {ComposeConnectionDeleteWarning(connections)}?";



    private string ComposeConnectionDeleteWarning(IEnumerable<ConnectionViewModel> connections)
    {
        var connectionStrings =
            connections
                .Select(x => $"#{x.Id}")
                .ToList();

        if (connectionStrings.Count == 1) return connectionStrings.Single();

        var stringsJoinedByComma = connectionStrings.Take(connectionStrings.Count - 1);
        var commaJoinedStrings = string.Join(", ", stringsJoinedByComma);
        return $"{commaJoinedStrings} and {connectionStrings.Last()}";
    }
}
