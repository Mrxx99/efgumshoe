﻿using EfGumshoe.DesktopAppLib.ViewModels;

namespace EfGumshoe.DesktopAppLib.Services;



public interface IConnectionDeleteConfirmer
{
    Result ShowDeleteWarning(ICollection<ConnectionViewModel> connections);
    Result ShowDeleteAllWarning();



    public enum Result
    {
        NotConfirmed,
        Confirmed
    }
}
