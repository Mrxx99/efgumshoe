﻿
# ===============================================
# Input variables
# ===============================================
$sourceFolder = "../EfGumshoe.DesktopApp/bin/Release/net6.0-windows"
$projectWxs = "Assets/Product.wxs"
$licenseFile = "Assets/LICENSE.rtf"
$iconFile = "Assets/logo.ico"
$msiFileName = "EF Gumshoe Installer.msi"


# ===============================================
# Derived variables
# ===============================================
$absoluteInputFolder = "$pwd/$sourceFolder"
$absoluteProjectWxs = "$pwd/$projectWxs"
$absoluteLicenseFile = "$pwd/$licenseFile"
$absoluteIconFile = "$pwd/$iconFile"


# ===============================================
# Refresh files
# ===============================================
$woxPath = "obj/wox"
if (test-path $woxPath) {
    remove-item $woxPath -Force -Recurse
}
mkdir $woxPath

Push-Location $woxPath

Copy-Item -Path "$absoluteInputFolder/*" -Recurse -Force


# ===============================================
# Harvest files
# ===============================================

$filesWxs = "files.wxs"
heat dir . -cg ProductFilesComponentGroup -dr INSTALLLOCATION -o $filesWxs -gg -srd -sfrag -sreg


# ===============================================
# Create MSI file
# ===============================================
$xml = New-Object XML
$xml.Load("$pwd/$filesWxs")
$nsm = New-Object Xml.XmlNamespaceManager($xml.NameTable)
$nsm.AddNamespace('ns', $xml.DocumentElement.NamespaceURI)
$component = $xml.SelectSingleNode("//ns:File[@Source = 'SourceDir\EfGumshoe.exe']", $nsm)
$exeId= $component.Id

Copy-Item -Path "$absoluteLicenseFile" -Destination "LICENSE.rtf" -Force
Copy-Item -Path "$absoluteIconFile" -Destination "icon.ico" -Force

candle $absoluteProjectWxs -o "project.wixobj" -arch x64 -dMainExe="$exeId"
candle $filesWxs -o "files.wixobj" -arch x64
light "project.wixobj" "files.wixobj" -o $msiFileName -ext WixUIExtension -cultures:en-us

Pop-Location


# ===============================================
# Copy results
# ===============================================
if (!(test-path "bin")) {
    mkdir "bin"
}
Copy-Item -Path "$woxPath/$msiFileName" -Destination "bin/"

