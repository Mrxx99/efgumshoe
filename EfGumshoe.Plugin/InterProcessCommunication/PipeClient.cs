﻿using System.IO.Pipes;
using System.Security.Principal;
using Microsoft.Extensions.Logging;

namespace EfGumshoe.Plugin.InterProcessCommunication;



public interface IPipeClient
{
    Task SendMessage(string message);
}



public class PipeClient : IPipeClient
{
    private readonly ILogger<PipeClient> _logger;

    public PipeClient(ILogger<PipeClient> logger)
    {
        _logger = logger;
    }

    async Task IPipeClient.SendMessage(string message)
    {
        var pipeClient =
            new NamedPipeClientStream(
                ".",
                EfGumshoeConstants.PipeName,
                PipeDirection.InOut,
                PipeOptions.None,
                TokenImpersonationLevel.Impersonation
            );

        try
        {
            await pipeClient.ConnectAsync(30);
        }
        catch (Exception)
        {
            _logger.LogDebug("EfGumshoe desktop app not found");
            pipeClient.Close();
            return;
        }


        var streamString = new StreamString(pipeClient);

        if (streamString.ReadString() == EfGumshoeConstants.Watchword)
        {
            streamString.WriteString(message);
        }
        else
        {
            _logger.LogDebug("EfGumshoe desktop app could not be verified");
        }

        pipeClient.Close();
    }
}
