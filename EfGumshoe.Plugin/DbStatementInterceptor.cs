﻿using System.Data.Common;
using EfGumshoe.Plugin.InterProcessCommunication;
using EfGumshoe.Plugin.JsonModels;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Newtonsoft.Json;

namespace EfGumshoe.Plugin;



public interface IDbStatementInterceptor : IInterceptor
{
}



public class DbStatementInterceptor : DbCommandInterceptor, IDbStatementInterceptor
{
    private readonly IPipeClient _pipeClient;



    public DbStatementInterceptor(IPipeClient pipeClient)
    {
        _pipeClient = pipeClient;
    }



    public override InterceptionResult DataReaderDisposing(
        DbCommand command,
        DataReaderDisposingEventData eventData,
        InterceptionResult result
    )
    {
        var jsonModel = new MessageJsonModel
        {
            SentDate = DateTime.Now,
            Version = 1,
            ConnectionHashCode = command.Connection!.GetHashCode(),
            TransactionHashCode = command.Transaction?.GetHashCode(),
            CommandType = command.CommandType,
            CommandText = command.CommandText,
            Parameters =
                command
                    .Parameters
                    .Cast<DbParameter>()
                    .Select(x =>
                        new ParameterJsonModel
                        {
                            Name = x.ParameterName,
                            Value = x.Value
                        }
                    )
                    .ToList(),
            ReadCount = eventData.ReadCount,
            Duration = eventData.Duration
        };


        var jsonString = JsonConvert.SerializeObject(jsonModel);
        _pipeClient.SendMessage(jsonString).GetAwaiter().GetResult();


        return base.DataReaderDisposing(command, eventData, result);
    }
}
