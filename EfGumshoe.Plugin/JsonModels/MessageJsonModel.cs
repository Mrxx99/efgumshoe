﻿using System.Data;
using Newtonsoft.Json;

namespace EfGumshoe.Plugin.JsonModels;



[JsonObject(MemberSerialization.OptIn)]
public class MessageJsonModel
{
    [JsonProperty("version")]
    public int Version { get; set; }

    [JsonProperty("sentDate")]
    public DateTime SentDate { get; set; }

    [JsonProperty("connection")]
    public int ConnectionHashCode { get; set; }

    [JsonProperty("transaction")]
    public int? TransactionHashCode { get; set; }

    [JsonProperty("commandType")]
    public CommandType CommandType { get; set; }

    [JsonProperty("commandText")]
    public string? CommandText { get; set; }

    [JsonProperty("parameters")]
    public List<ParameterJsonModel> Parameters { get; set; } = new();

    [JsonProperty("readCount")]
    public int ReadCount { get; set; }

    [JsonProperty("duration")]
    public TimeSpan Duration { get; set; }
}
