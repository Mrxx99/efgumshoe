﻿using Newtonsoft.Json;

namespace EfGumshoe.Plugin.JsonModels;



[JsonObject(MemberSerialization.OptIn)]
public class ParameterJsonModel
{
    [JsonProperty("name")]
    public string Name { get; set; } = null!;

    [JsonProperty("value")]
    public object? Value { get; set; }
}
