﻿using System;
using System.Linq;
using EfGumshoe.DesktopApp.Shared.InterProcessCommunication;
using EfGumshoe.DesktopAppLib.ViewModels;
using EfGumshoe.Plugin.JsonModels;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EfGumshoe.DesktopApp.Shared;



public interface IClientMessageProcessor
{
    void Process(object? sender, IAsyncPipeHost.MessageReceivedEventArgs e);
}



public class ClientMessageProcessor : IClientMessageProcessor, ITransientDependency
{
    private readonly ILogger<ClientMessageProcessor> _logger;
    private readonly IMainWindowViewModel _mainWindowViewModel;
    private readonly IConnectionViewModelFactory _connectionViewModelFactory;



    public ClientMessageProcessor(
        ILogger<ClientMessageProcessor> logger,
        IMainWindowViewModel mainWindowViewModel,
        IConnectionViewModelFactory connectionViewModelFactory
    )
    {
        _logger = logger;
        _mainWindowViewModel = mainWindowViewModel;
        _connectionViewModelFactory = connectionViewModelFactory;
    }



    public void Process(object? sender, IAsyncPipeHost.MessageReceivedEventArgs eventArgs)
    {
        var message = eventArgs.Message;

        _logger.LogInformation("Processing JSON message: {Message}", message);


        var messageJsonModel = DeserializeJsonModel(message);


        var connection = GetConnection(messageJsonModel.ConnectionHashCode);
        var transaction = GetTransaction(messageJsonModel.TransactionHashCode, connection);

        connection.AddStatement(
            new StatementViewModel
            (
                transaction: transaction,
                commandText: messageJsonModel.CommandText,
                sentDate: messageJsonModel.SentDate,
                parameters:
                messageJsonModel.Parameters
                    .Select(x => new ParameterViewModel(x.Name, x.Value))
                    .ToList(),
                rowCount: messageJsonModel.ReadCount,
                duration: messageJsonModel.Duration
            )
        );


        _logger.LogInformation("Message processed");
    }



    private MessageJsonModel DeserializeJsonModel(string message)
    {
        try
        {
            return JsonConvert.DeserializeObject<MessageJsonModel>(message)!;
        }
        catch (Exception)
        {
            _logger.LogError("Can not convert JSON: {Message}", message);
            throw;
        }
    }



    private ConnectionViewModel GetConnection(int connectionHashCode)
    {
        var existingConnection = _mainWindowViewModel.GetByHashCode(connectionHashCode);
        if (existingConnection != null) return existingConnection;

        var newConnection = _connectionViewModelFactory.Create(connectionHashCode);
        _mainWindowViewModel.AddConnection(newConnection);
        return newConnection;
    }



    private TransactionViewModel? GetTransaction(int? transactionHashCode, ConnectionViewModel connection)
    {
        if (transactionHashCode == null) return null;

        var existingTransaction = connection.GetTransactionByHashCode(transactionHashCode);
        if (existingTransaction != null) return existingTransaction;


        var newTransaction = new TransactionViewModel(transactionHashCode.Value);
        connection.AddTransaction(newTransaction);
        return newTransaction;
    }
}
