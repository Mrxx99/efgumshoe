﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Threading.Tasks;
using System.Windows.Threading;
using EfGumshoe.Plugin;

namespace EfGumshoe.DesktopApp.Shared.InterProcessCommunication;



public interface IAsyncPipeHost
{
    event EventHandler<MessageReceivedEventArgs> MessageReceived;
    void StartListening(Dispatcher dispatcher);



    public class MessageReceivedEventArgs : EventArgs
    {
        public MessageReceivedEventArgs(string message)
        {
            Message = message;
        }


        public string Message { get; }
    }
}



public class AsyncPipeHost : IAsyncPipeHost, ISingletonDependency
{
    private readonly NamedPipeServerStream _pipeServer;


    public AsyncPipeHost()
    {
        _pipeServer = new NamedPipeServerStream(EfGumshoeConstants.PipeName, PipeDirection.InOut);
    }


    public event EventHandler<IAsyncPipeHost.MessageReceivedEventArgs>? MessageReceived;


    public void StartListening(Dispatcher dispatcher)
    {
        Task.Run(() => KeepListening(dispatcher));
    }


    private async Task KeepListening(Dispatcher dispatcher)
    {
        while (true)
        {
            await WaitForAndProcessNextMessage(dispatcher);
        }
        // ReSharper disable once FunctionNeverReturns
    }


    private async Task WaitForAndProcessNextMessage(Dispatcher dispatcher)
    {
        await _pipeServer.WaitForConnectionAsync();

        try
        {
            var streamString = new StreamString(_pipeServer);

            streamString.WriteString(EfGumshoeConstants.Watchword);

            var jsonString = streamString.ReadString();
            var eventArgs = new IAsyncPipeHost.MessageReceivedEventArgs(jsonString);
            dispatcher.Invoke(() => MessageReceived?.Invoke(this, eventArgs));
        }
        catch (IOException e)
        {
            Console.WriteLine("ERROR: {0}", e.Message);
        }

        _pipeServer.Disconnect();
    }
}
