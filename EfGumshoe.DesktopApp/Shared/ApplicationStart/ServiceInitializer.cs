﻿using System.Windows.Threading;
using EfGumshoe.DesktopApp.Shared.InterProcessCommunication;

namespace EfGumshoe.DesktopApp.Shared.ApplicationStart;



public interface IServiceInitializer
{
    void Initialize(Dispatcher dispatcher);
}



public class ServiceInitializer : IServiceInitializer, ITransientDependency
{
    private readonly IAsyncPipeHost _asyncPipeHost;
    private readonly IClientMessageProcessor _clientMessageProcessor;



    public ServiceInitializer(
        IAsyncPipeHost asyncPipeHost,
        IClientMessageProcessor clientMessageProcessor
    )
    {
        _asyncPipeHost = asyncPipeHost;
        _clientMessageProcessor = clientMessageProcessor;
    }



    public void Initialize(Dispatcher dispatcher)
    {
        _asyncPipeHost.MessageReceived += _clientMessageProcessor.Process;
        _asyncPipeHost.StartListening(dispatcher);
    }
}
