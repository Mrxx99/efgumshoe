﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using EfGumshoe.DesktopAppLib.Services;
using EfGumshoe.DesktopAppLib.ViewModels;

namespace EfGumshoe.DesktopApp.Windows.MainWindow.Services;



public class ConnectionDeleteConfirmer : IConnectionDeleteConfirmer, ITransientDependency
{
    private readonly IDeleteConnectionWarningFormatter _deleteConnectionWarningFormatter;



    public ConnectionDeleteConfirmer(IDeleteConnectionWarningFormatter deleteConnectionWarningFormatter)
    {
        _deleteConnectionWarningFormatter = deleteConnectionWarningFormatter;
    }



    IConnectionDeleteConfirmer.Result IConnectionDeleteConfirmer.ShowDeleteWarning(
        ICollection<ConnectionViewModel> connections
    )
    {
        if (!connections.Any())
        {
            return
                MessageBox.Show("No connections selected!", "Remove Confirmation") == MessageBoxResult.Yes
                    ? IConnectionDeleteConfirmer.Result.Confirmed
                    : IConnectionDeleteConfirmer.Result.NotConfirmed;
        }

        return MessageBox.Show(
            _deleteConnectionWarningFormatter.ComposeWarningMessage(connections),
            "Remove Confirmation",
            MessageBoxButton.YesNo
        ) == MessageBoxResult.Yes
            ? IConnectionDeleteConfirmer.Result.Confirmed
            : IConnectionDeleteConfirmer.Result.NotConfirmed;
        ;
    }



    public IConnectionDeleteConfirmer.Result ShowDeleteAllWarning() =>
        MessageBox.Show(
            "Do you really want to remove all connections?",
            "Remove Confirmation",
            MessageBoxButton.YesNo
        ) == MessageBoxResult.Yes
            ? IConnectionDeleteConfirmer.Result.Confirmed
            : IConnectionDeleteConfirmer.Result.NotConfirmed;
}
