﻿using System.Windows;
using EfGumshoe.DesktopAppLib.ViewModels;

namespace EfGumshoe.DesktopApp.Windows.MainWindow.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(IMainWindowViewModel mainWindowViewModel)
        {
            InitializeComponent();
            DataContext = mainWindowViewModel;
        }
    }
}
