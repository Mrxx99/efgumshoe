﻿using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using ScintillaNET;

namespace EfGumshoe.DesktopApp.Windows.MainWindow.Views;



public interface IStatementDetailView
{
}



public partial class StatementDetailView : UserControl, IStatementDetailView
{
    public StatementDetailView()
    {
        InitializeComponent();

        Scintilla = new Scintilla();

        Scintilla.Dock = System.Windows.Forms.DockStyle.Fill;
        Scintilla.CaretForeColor = Color.White;

        Scintilla.Margins[0].Width = 16;

        Scintilla.StyleResetDefault();
        Scintilla.Styles[ScintillaNET.Style.Default].Font = "Consolas";
        Scintilla.Styles[ScintillaNET.Style.Default].Size = 10;
        Scintilla.Styles[ScintillaNET.Style.Default].BackColor = Color.FromArgb(41, 49, 52);
        Scintilla.StyleClearAll();

        Scintilla.LexerName = "sql";

        Scintilla.Styles[ScintillaNET.Style.Sql.Comment].ForeColor = Color.SlateGray;
        Scintilla.Styles[ScintillaNET.Style.Sql.CommentLine].ForeColor = Color.SlateGray;
        Scintilla.Styles[ScintillaNET.Style.Sql.CommentDoc].ForeColor = Color.SlateGray;
        Scintilla.Styles[ScintillaNET.Style.Sql.Number].ForeColor = Color.Orange;
        Scintilla.Styles[ScintillaNET.Style.Sql.Word].ForeColor = Color.Chartreuse;
        Scintilla.Styles[ScintillaNET.Style.Sql.Word].Bold = true;
        Scintilla.Styles[ScintillaNET.Style.Sql.Character].ForeColor = Color.Chocolate;
        Scintilla.Styles[ScintillaNET.Style.Sql.Operator].ForeColor = Color.Aquamarine;
        Scintilla.Styles[ScintillaNET.Style.Sql.Identifier].ForeColor = Color.White;
        Scintilla.Styles[ScintillaNET.Style.Sql.SqlPlusComment].ForeColor = Color.SlateGray;
        Scintilla.Styles[ScintillaNET.Style.Sql.CommentLineDoc].ForeColor = Color.SlateGray;
        Scintilla.Styles[ScintillaNET.Style.Sql.Word2].ForeColor = Color.Fuchsia;
        Scintilla.Styles[ScintillaNET.Style.Sql.User1].ForeColor = Color.Orchid;
        Scintilla.Styles[ScintillaNET.Style.Sql.User2].ForeColor = Color.Orchid;
        Scintilla.Styles[ScintillaNET.Style.Sql.User3].ForeColor = Color.Orchid;
        Scintilla.Styles[ScintillaNET.Style.Sql.User4].ForeColor = Color.Orchid;

        Scintilla.SetKeywords(
            0,
            "add alter as authorization backup begin bigint binary bit break browse bulk by cascade case " +
            "catch check checkpoint close clustered column commit compute constraint containstable continue create " +
            "current cursor cursor database date datetime datetime2 datetimeoffset dbcc deallocate decimal declare " +
            "default delete deny desc disk distinct distributed double drop dump else end errlvl escape except exec " +
            "execute exit external fetch file fillfactor float for foreign freetext freetexttable from full function " +
            "goto grant group having hierarchyid holdlock identity identity_insert identitycol if image index insert " +
            "int intersect into key kill lineno load merge money national nchar nocheck nocount nolock nonclustered " +
            "ntext numeric nvarchar of off offsets on open opendatasource openquery openrowset openxml option order " +
            "over percent plan precision primary print proc procedure public raiserror read readtext real " +
            "reconfigure references replication restore restrict return revert revoke rollback rowcount rowguidcol " +
            "rule save schema securityaudit select set setuser shutdown smalldatetime smallint smallmoney " +
            "sql_variant statistics table table tablesample text textsize then time timestamp tinyint to top tran " +
            "transaction trigger truncate try union unique uniqueidentifier update updatetext use user values " +
            "varbinary varchar varying view waitfor when where while with writetext xml go @ "
        );

        Scintilla.SetKeywords(
            1,
            "ascii cast char charindex ceiling coalesce collate contains convert current_date " +
            "current_time current_timestamp current_user floor isnull max min nullif object_id session_user " +
            "substring system_user tsequal "
        );

        Scintilla.SetKeywords(
            4,
            "all and any between cross exists in inner is join left like not " +
            "null or outer pivot right some unpivot ( ) * "
        );

        Scintilla.SetKeywords(5, "sys objects sysobjects ");

        Scintilla.ReadOnly = true;


        WinFormsHost.Child = Scintilla;
    }



    private Scintilla Scintilla { get; }



    private void OnTargetUpdated(object? sender, DataTransferEventArgs e)
    {
        var propertyValue = e.TargetObject.GetValue(e.Property);
        Scintilla.ReadOnly = false;
        Scintilla.Text = $"{propertyValue}";
        Scintilla.ReadOnly = true;
    }
}
