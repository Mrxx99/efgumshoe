﻿using System.Windows.Controls;

namespace EfGumshoe.DesktopApp.Components;



public partial class Header : UserControl
{
    public Header()
    {
        InitializeComponent();
    }



    public string? Title
    {
        set => TitleLabel.Content = value;
    }
}
