using System;
using System.ComponentModel;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace EfGumshoe.DesktopApp;



/// <summary>
/// Marker interface for dependency injection
/// </summary>
internal interface IScopedDependency
{
}



/// <summary>
/// Marker interface for dependency injection
/// </summary>
internal interface ISingletonDependency
{
}



/// <summary>
/// Marker interface for dependency injection
/// </summary>
internal interface ITransientDependency
{
}



public static class MarkerInterfaceRegistrar
{
    public static void AddMarkerInterfaces(this IServiceCollection services)
    {
        services.RegisterMarkerInterfaces(typeof(ISingletonDependency), ServiceLifetime.Singleton);
        services.RegisterMarkerInterfaces(typeof(IScopedDependency), ServiceLifetime.Scoped);
        services.RegisterMarkerInterfaces(typeof(ITransientDependency), ServiceLifetime.Transient);
    }



    private static void RegisterMarkerInterfaces(
        this IServiceCollection serviceCollection,
        Type markerInterface,
        ServiceLifetime lifetime
    )
    {
        var assembly = markerInterface.Assembly;
        foreach (var implementation in assembly.DefinedTypes)
        {
            ImplementAppropriateTypes(serviceCollection, markerInterface, lifetime, implementation);
        }
    }



    private static void ImplementAppropriateTypes(
        IServiceCollection serviceCollection,
        Type markerInterface,
        ServiceLifetime lifetime,
        Type implementation
    )
    {
        var interfaces =
            implementation
                .GetInterfaces()
                .Where(x => x != typeof(INotifyPropertyChanged) && x != typeof(INotifyPropertyChanging))
                .ToHashSet();

        if (!interfaces.Contains(markerInterface)) return;

        if (interfaces.Count == 1)
        {
            serviceCollection.Add(new ServiceDescriptor(implementation, implementation, lifetime));
        }

        else if (interfaces.Count == 2)
        {
            var interfaceToImplement = interfaces.Single(x => x != markerInterface);
            serviceCollection.Add(new ServiceDescriptor(interfaceToImplement, implementation, lifetime));
        }

        else throw new NotSupportedException();
    }
}
