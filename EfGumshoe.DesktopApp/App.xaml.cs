﻿using System.Windows;
using EfGumshoe.DesktopApp.Shared.ApplicationStart;
using EfGumshoe.DesktopApp.Windows.MainWindow.Views;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace EfGumshoe.DesktopApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddLogging(configure => configure.AddConsole());
            serviceCollection.AddMarkerInterfaces();
            DesktopAppLib.MarkerInterfaceRegistrar.AddMarkerInterfaces(serviceCollection);
            serviceCollection.AddSingleton<MainWindow>();

            var services = serviceCollection.BuildServiceProvider();

            var serviceInitializer = services.GetRequiredService<IServiceInitializer>();
            serviceInitializer.Initialize(Dispatcher);

            var mainWindow = services.GetRequiredService<MainWindow>();
            mainWindow.Show();
        }
    }
}
