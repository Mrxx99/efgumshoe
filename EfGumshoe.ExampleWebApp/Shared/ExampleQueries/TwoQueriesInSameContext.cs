﻿using EfGumshoe.ExampleWebApp.Shared.Persistence;
using EfGumshoe.ExampleWebApp.Shared.Persistence.Models;
using Microsoft.EntityFrameworkCore;

namespace EfGumshoe.ExampleWebApp.Shared.ExampleQueries;



public interface ITwoQueriesInSameContext
{
    Task ExecuteQueryAsync(CancellationToken cancellationToken);
}



public class TwoQueriesInSameContext : ITwoQueriesInSameContext, IScopedDependency
{
    private readonly TestDbContext _testDbContext;



    public TwoQueriesInSameContext(TestDbContext testDbContext)
    {
        _testDbContext = testDbContext;
    }



    async Task ITwoQueriesInSameContext.ExecuteQueryAsync(CancellationToken cancellationToken)
    {
        await _testDbContext
            .Enrollments
            .Where(x =>
                x.Grade == Grade.C &&
                x.CourseId != 7
            )
            .Select(
                x => new
                {
                    x.Course.Title,
                    x.Student.LastName
                }
            )
            .ToListAsync(cancellationToken);

        await _testDbContext
            .Courses
            .Where(x =>
                x.Enrollments.Any(y => y.StudentId == 1) &&
                x.Credits < 3 &&
                x.Title.Length > 4
            )
            .Select(
                x => new
                {
                    x.Id,
                    x.Title,
                    x.Credits
                }
            )
            .ToListAsync(cancellationToken);
    }
}
