﻿using EfGumshoe.ExampleWebApp.Shared.Persistence;
using EfGumshoe.ExampleWebApp.Shared.Persistence.Models;
using Microsoft.EntityFrameworkCore;

namespace EfGumshoe.ExampleWebApp.Shared.ExampleQueries;



public interface ITransactionExampleQuery
{
    Task ExecuteQueriesInTransactionAsync(CancellationToken cancellationToken);
}



public class TransactionTransactionExampleQuery : ITransactionExampleQuery, IScopedDependency
{
    private readonly TestDbContext _testDbContext;



    public TransactionTransactionExampleQuery(TestDbContext testDbContext)
    {
        _testDbContext = testDbContext;
    }



    async Task ITransactionExampleQuery.ExecuteQueriesInTransactionAsync(CancellationToken cancellationToken)
    {
        await _testDbContext
            .Enrollments
            .Where(x =>
                x.Grade == Grade.C &&
                x.CourseId != 7
            )
            .ToListAsync(cancellationToken);

        await _testDbContext
            .Courses
            .Where(x =>
                x.Enrollments.Any(y => y.StudentId == 1) &&
                x.Credits < 3 &&
                x.Title.Length > 4
            )
            .ToListAsync(cancellationToken);


        await ExecuteInTransaction(cancellationToken);
        await ExecuteInTransaction(cancellationToken);


        await _testDbContext
            .Courses
            .Where(x => x.Credits < 0)
            .ToListAsync(cancellationToken);


        await ExecuteInTransaction(cancellationToken);
    }



    private async Task ExecuteInTransaction(CancellationToken cancellationToken)
    {
        await using var transaction = await _testDbContext.Database.BeginTransactionAsync(cancellationToken);

        var course = new Course
        {
            Title = "Course to learn stuff",
            Credits = 3
        };

        _testDbContext.Courses.Add(course);

        await _testDbContext.SaveChangesAsync(cancellationToken);


        _testDbContext
            .Students
            .Add(
                new Student
                {
                    FirstMidName = "Aristotle",
                    LastName = "The Greek guy",
                    EnrollmentDate = DateTime.Now
                }
            );

        await _testDbContext.SaveChangesAsync(cancellationToken);

        await transaction.CommitAsync(cancellationToken);
    }
}
