﻿using EfGumshoe.ExampleWebApp.Shared.Persistence;

namespace EfGumshoe.ExampleWebApp.Shared.ExampleQueries;



public interface ISyncQuery
{
    Task Execute(CancellationToken cancellationToken);
}



public class SyncQuery : ISyncQuery, IScopedDependency
{
    private readonly TestDbContext _testDbContext;



    public SyncQuery(TestDbContext testDbContext)
    {
        _testDbContext = testDbContext;
    }



    Task ISyncQuery.Execute(CancellationToken cancellationToken)
    {
        _testDbContext
            .Courses
            .Where(x => x.Credits > 0)
            .Select(
                x => new
                {
                    x.Title,
                    x.Credits
                }
            )
            // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
            .ToList();

        return Task.CompletedTask;
    }
}
