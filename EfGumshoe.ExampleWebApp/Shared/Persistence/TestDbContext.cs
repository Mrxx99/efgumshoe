﻿using EfGumshoe.ExampleWebApp.Shared.Persistence.Models;
using Microsoft.EntityFrameworkCore;

namespace EfGumshoe.ExampleWebApp.Shared.Persistence;



public class TestDbContext : DbContext
{
    protected TestDbContext()
    {
    }

    public TestDbContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<Course> Courses { get; set; } = null!;
    public DbSet<Enrollment> Enrollments { get; set; } = null!;
    public DbSet<Student> Students { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(TestDbContext).Assembly);
    }
}
