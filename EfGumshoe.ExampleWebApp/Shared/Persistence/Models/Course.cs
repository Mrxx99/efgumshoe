﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfGumshoe.ExampleWebApp.Shared.Persistence.Models;



public class Course
{
    public int Id { get; set; }
    public string Title { get; set; } = null!;
    public int Credits { get; set; }

    public ICollection<Enrollment> Enrollments { get; set; } = null!;
}



public class ModeratorFlagConfiguration : IEntityTypeConfiguration<Course>
{
    public void Configure(EntityTypeBuilder<Course> entity)
    {
        entity.ToTable("courses");

        entity
            .HasKey(e => e.Id)
            .HasName("id");

        entity
            .Property(e => e.Title)
            .HasColumnName("title");

        entity
            .Property(e => e.Credits)
            .HasColumnName("credits");


        entity
            .HasMany<Enrollment>()
            .WithOne(x => x.Course)
            .HasForeignKey(x => x.CourseId);
    }
}
