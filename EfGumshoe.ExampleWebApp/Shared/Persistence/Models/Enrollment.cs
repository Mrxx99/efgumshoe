﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfGumshoe.ExampleWebApp.Shared.Persistence.Models;



public enum Grade
{
    A, B, C, D, F
}



public class Enrollment
{
    public int Id { get; set; }

    public int CourseId { get; set; }
    public Course Course { get; set; } = null!;

    public Grade? Grade { get; set; }

    public int StudentId { get; set; }
    public Student Student { get; set; } = null!;
}



public class EnrollmentConfiguration : IEntityTypeConfiguration<Enrollment>
{
    public void Configure(EntityTypeBuilder<Enrollment> entity)
    {
        entity.ToTable("enrollments");

        entity
            .HasKey(e => e.Id)
            .HasName("idp");


        entity
            .Property(e => e.CourseId)
            .HasColumnName("course");
        entity
            .HasOne(d => d.Course)
            .WithMany(p => p.Enrollments)
            .HasForeignKey(d => d.CourseId)
            .OnDelete(DeleteBehavior.ClientSetNull)
            .HasConstraintName("FK__enrollment__course");

        entity
            .Property(e => e.StudentId)
            .HasColumnName("student");
        entity
            .HasOne(d => d.Student)
            .WithMany(p => p.Enrollments)
            .HasForeignKey(d => d.StudentId)
            .OnDelete(DeleteBehavior.ClientSetNull)
            .HasConstraintName("FK__enrollment__student");

        entity
            .Property(e => e.Grade)
            .HasColumnName("grade");
    }
}
