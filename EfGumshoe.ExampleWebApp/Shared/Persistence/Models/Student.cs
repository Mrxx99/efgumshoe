﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EfGumshoe.ExampleWebApp.Shared.Persistence.Models;



public class Student
{
    public int Id { get; set; }

    public string FirstMidName { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public DateTime EnrollmentDate { get; set; }


    public ICollection<Enrollment> Enrollments { get; set; } = null!;
}



public class StudentConfiguration : IEntityTypeConfiguration<Student>
{
    public void Configure(EntityTypeBuilder<Student> entity)
    {
        entity.ToTable("students");

        entity
            .HasKey(e => e.Id)
            .HasName("idi");


        entity
            .Property(e => e.FirstMidName)
            .HasColumnName("first_name");

        entity
            .Property(e => e.LastName)
            .HasColumnName("last_name");

        entity
            .Property(e => e.EnrollmentDate)
            .HasColumnName("enrollmentDate");

        entity
            .HasMany<Enrollment>()
            .WithOne(x => x.Student)
            .HasForeignKey(x => x.StudentId);
    }
}
