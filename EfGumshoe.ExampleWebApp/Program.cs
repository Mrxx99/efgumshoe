using EfGumshoe.ExampleWebApp;
using EfGumshoe.ExampleWebApp.Shared.Persistence;
using EfGumshoe.Extensions.AspCore;
using EfGumshoe.Plugin;
using Microsoft.EntityFrameworkCore;


var builder = WebApplication.CreateBuilder(args);


builder.Services.AddRazorPages();
builder.Services.AddMarkerInterfaces();

builder.Services
    .AddDbContext<TestDbContext>(
        (provider, options) =>
            options
                .UseSqlServer(builder.Configuration.GetConnectionString("WebApp"))
                .UseEfGumshoe(provider, builder.Environment.IsDevelopment())
                .EnableSensitiveDataLogging()
    );

builder.Services.AddEfGumshoe();

builder.Services.AddScoped<DbStatementInterceptor>();


var app = builder.Build();


if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();


var database = app
    .Services
    .CreateScope()
    .ServiceProvider
    .GetRequiredService<TestDbContext>()
    .Database;

database.EnsureDeleted();
database.EnsureCreated();


app.Run();
