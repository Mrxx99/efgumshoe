﻿using EfGumshoe.ExampleWebApp.Shared.ExampleQueries;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EfGumshoe.ExampleWebApp.Pages;



public class IndexModel : PageModel
{
    public IndexModel(
        ISyncQuery syncQuery,
        ITwoQueriesInSameContext twoQueriesInSameContext,
        ITransactionExampleQuery transactionExampleQuery
    )
    {
        ExampleQueries =
            new Dictionary<int, Func<CancellationToken, Task>>
            {
                [0] = syncQuery.Execute,
                [1] = twoQueriesInSameContext.ExecuteQueryAsync,
                [2] = transactionExampleQuery.ExecuteQueriesInTransactionAsync
            };
    }



    public Dictionary<int, Func<CancellationToken, Task>> ExampleQueries { get; set; }

    public Dictionary<int, string> QueryNames { get; } = new()
    {
        [0] = "Synchronous query",
        [1] = "2 queries in same context",
        [2] = "Multiple transactions"
    };

    public string Message { get; set; } = "Initial Request";



    public async Task OnPostRunQuery(int id, CancellationToken cancellationToken)
    {
        await ExampleQueries[id].Invoke(cancellationToken);
        Message = $"Query {id} executed";
    }
}
