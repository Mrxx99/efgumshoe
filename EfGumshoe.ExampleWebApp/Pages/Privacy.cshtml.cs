﻿using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EfGumshoe.ExampleWebApp.Pages;



public class PrivacyModel : PageModel
{
    private readonly ILogger<PrivacyModel> _logger;


    public PrivacyModel(ILogger<PrivacyModel> logger)
    {
        _logger = logger;
    }


    public void OnGet()
    {
    }
}
