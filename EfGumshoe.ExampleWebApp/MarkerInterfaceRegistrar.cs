namespace EfGumshoe.ExampleWebApp;



/// <summary>
/// Marker interface for dependency injection
/// </summary>
internal interface IScopedDependency
{
}



/// <summary>
/// Marker interface for dependency injection
/// </summary>
internal interface ISingletonDependency
{
}



/// <summary>
/// Marker interface for dependency injection
/// </summary>
internal interface ITransientDependency
{
}


public static class MarkerInterfaceRegistrar
{
	public static void AddMarkerInterfaces(this IServiceCollection services)
	{
		services.RegisterMarkerInterfaces(typeof(ISingletonDependency), ServiceLifetime.Singleton);
		services.RegisterMarkerInterfaces(typeof(IScopedDependency), ServiceLifetime.Scoped);
		services.RegisterMarkerInterfaces(typeof(ITransientDependency), ServiceLifetime.Transient);
	}


	private static void RegisterMarkerInterfaces(
		this IServiceCollection serviceCollection,
		Type markerInterface,
		ServiceLifetime lifetime
	)
	{
		var assembly = markerInterface.Assembly;
		foreach (var implementation in assembly.DefinedTypes)
		{
			ImplementAppropriateTypes(serviceCollection, markerInterface, lifetime, implementation);
		}
	}


	private static void ImplementAppropriateTypes(
		IServiceCollection serviceCollection,
		Type markerInterface,
		ServiceLifetime lifetime,
		Type implementation
	)
	{
		var interfaces = implementation.GetInterfaces();
		if (!interfaces.Contains(markerInterface)) return;

		if (interfaces.Length == 1)
		{
			serviceCollection.Add(new ServiceDescriptor(implementation, implementation, lifetime));
		}

		else if (interfaces.Length == 2)
		{
			var interfaceToImplement = interfaces.Single(x => x != markerInterface);
			serviceCollection.Add(new ServiceDescriptor(interfaceToImplement, implementation, lifetime));
		}

		else throw new NotSupportedException();
	}
}
