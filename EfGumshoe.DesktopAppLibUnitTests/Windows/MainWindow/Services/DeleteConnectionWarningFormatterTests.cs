using System;
using EfGumshoe.DesktopAppLib.Services;
using EfGumshoe.DesktopAppLib.ViewModels;
using Xunit;

namespace EfGumshoe.DesktopAppLibUnitTests.Windows.MainWindow.Services;



public class DeleteConnectionWarningFormatterTests
{
    [Fact]
    public void ComposeWarningMessage_SingleEntry_FormatsCorrectly()
    {
        // Arrange
        var formatter = Create();
        var connections = new[] { new ConnectionViewModel(1, 1, DateTime.UnixEpoch) };


        // Act
        var result = formatter.ComposeWarningMessage(connections);


        // Assert
        Assert.Equal($"Do you really want to remove connections #1?", result);
    }



    [Fact]
    public void ComposeWarningMessage_TwoEntries_CombinedWithAnd()
    {
        // Arrange
        var formatter = Create();
        var connections = new[]
        {
            new ConnectionViewModel(1, 1, DateTime.UnixEpoch),
            new ConnectionViewModel(2, 2, DateTime.UnixEpoch)
        };


        // Act
        var result = formatter.ComposeWarningMessage(connections);


        // Assert
        Assert.Equal($"Do you really want to remove connections #1 and #2?", result);
    }



    [Fact]
    public void ComposeWarningMessage_ThreeEntries_CombinedWithCommaAndAnd()
    {
        // Arrange
        var formatter = Create();
        var connections = new[]
        {
            new ConnectionViewModel(1, 1, DateTime.UnixEpoch),
            new ConnectionViewModel(2, 2, DateTime.UnixEpoch),
            new ConnectionViewModel(3, 3, DateTime.UnixEpoch)
        };


        // Act
        var result = formatter.ComposeWarningMessage(connections);


        // Assert
        Assert.Equal($"Do you really want to remove connections #1, #2 and #3?", result);
    }



    private IDeleteConnectionWarningFormatter Create() => new DeleteConnectionWarningFormatter();
}
